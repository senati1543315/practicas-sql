# PRACTICAS SQL
## estudiante elvis winder ccama aycaya
# curso : database programing with sql
[1. crear tablas(create tabla _ all_tables - drop table)](solucionario/solucionario_1.md)

[2. ingresar registros ](solucionario/solucionario_2.md)

[3. Tipos de datos](solucionario/solucionario_3.md)

[4. Recuperar algunos campos ](solucionario/solucionario_4.md)

[5. Recuperar algunos registros (where)  ](solucionario/solucionario_5.md)

[6. Operadores relacionales ](solucionario/solucionario_6.md)

[7. Borrar registros  ](solucionario/solucionario_7.md)

[8. Actualizar registros ](solucionario/solucionario_8.md)

[10. Valores nulos](solucionario/solucionario_10.md)

[11. Operadores relacionales  ](solucionario/solucionario_11.md)

[12. Clave primaria ](solucionario/solucionario_12.md)

[13. Vaciar la tabla ](solucionario/solucionario_13.md)

[14. Tipos de datos alfanuméricos ](solucionario/solucionario_14.md)

[15. Tipos de datos numéricos](solucionario/solucionario_15.md)

[16. Ingresar algunos campos ](solucionario/solucionario_16.md)

[17. Valores por defecto ](solucionario/solucionario_17.md)

[18. Operadores aritméticos y de concatenación](solucionario/solucionario_18.md)

[19.  Alias (encabezados de columnas)](solucionario/solucionario_19.md)

[20. Funciones string ](solucionario/solucionario_20.md)

[21. Funciones matemáticas ](solucionario/solucionario_21.md)

[22. Funciones de fechas y horas ](solucionario/solucionario_22.md)

[23. Ordenar registros](solucionario/solucionario_23.md)

[24. Operadores lógicos ](solucionario/solucionario_24.md)

[25. Otros operadores relacionales](solucionario/solucionario_25.md)

[26. Otros operadores relacionales](solucionario/solucionario_26.md)

[27. Búsqueda de patrones  ](solucionario/solucionario_27.md)

[28. Contar registros  ](solucionario/solucionario_28.md)

[29. Funciones de grupo ](solucionario/solucionario_29.md)

[30. Agrupar registros ](solucionario/solucionario_30.md)

[31. Seleccionar grupos  ](solucionario/solucionario_31.md)

[32. Registros duplicados ](solucionario/solucionario_32.md)

[33. Clave primaria compuesta](solucionario/solucionario_33.md)

[34. Secuencias ](solucionario/solucionario_34.md)

[35. Alterar secuencia ](solucionario/solucionario_35.md)

[36. Integridad de datos](solucionario/solucionario_36.md)

[37. Restricción primary key ](solucionario/solucionario_37.md)

[38. Restricción unique](solucionario/solucionario_38.md)

[39. Restriccioncheck ](solucionario/solucionario_39.md)

[40. Restricciones: validación y estados](solucionario/solucionario_40.md)

[41. Restricciones: información ](solucionario/solucionario_41.md)

[42. Restricciones: eliminación ](solucionario/solucionario_42.md)

[43. Indices ](solucionario/solucionario_43.md)

[44. Indices (Crear . Información) ](solucionario/solucionario_44.md)

[45. Indices (eliminar)](solucionario/solucionario_45.md)

[46. Varias tablas (join) ](solucionario/solucionario_46.md)

[47. Combinación interna (join) ](solucionario/solucionario_47.md)

[48. Combinación externa izquierda (left join) ](solucionario/solucionario_48.md)

[49. Combinación externa derecha (right join)](solucionario/solucionario_49.md)

[50. Combinación externa completa (full join) ](solucionario/solucionario_50.md)

[51. Combinaciones cruzadas (cross)](solucionario/solucionario_51.md)

[52. Autocombinación ](solucionario/solucionario_52.md)

[53. Combinaciones y funciones de agrupamiento](solucionario/solucionario_53.md)

[54. Combinar más de 2 tablas ](solucionario/solucionario_54.md)

[55. Otros tipos de combinaciones](solucionario/solucionario_55.md)

[56. Clave foránea](solucionario/solucionario_56.md)

[57. Restricciones (foreign key)](solucionario/solucionario_57.md)

[58. Restricciones foreign key en la misma tabla ](solucionario/solucionario_58.md)

[59. Restricciones foreign key (eliminación)](solucionario/solucionario_59.md)

[60. Restricciones foreign key deshabilitar y validar](solucionario/solucionario_60.md)

[61. Restricciones foreign key (acciones)](solucionario/solucionario_61.md)

[62. Información de user_constraints](solucionario/solucionario_62.md)

[63. Restricciones al crear la tabla ](solucionario/solucionario_63.md)

[64. Unión](solucionario/solucionario_64.md)

[65. Intersección](solucionario/solucionario_65.md)

[66. Minus ](solucionario/solucionario_66.md)

[67. Agregar campos (alter table-add) ](solucionario/solucionario_67.md)

[68. Modificar campos (alter table - modify) ](solucionario/solucionario_68.md)

[69. Eliminar campos (alter table - drop) ](solucionario/solucionario_69.md)

[70.  Agregar campos y restricciones (alter table) ](solucionario/solucionario_70.md)

[71. Subconsultas ](solucionario/solucionario_71.md)

[72. Subconsultas como expresion ](solucionario/solucionario_72.md)

[73. Subconsultas con in ](solucionario/solucionario_73.md)

[74. Subconsultas any- some - all ](solucionario/solucionario_74.md)

[75. Subconsultas correlacionadas ](solucionario/solucionario_75.md)

[76. Exists y No Exists ](solucionario/solucionario_76.md)

[77. Subconsulta simil autocombinacion ](solucionario/solucionario_77.md)

[78. Subconsulta conupdate y delete ](solucionario/solucionario_78.md)

[79. Subconsulta e insert ](solucionario/solucionario_79.md)

[80. Crear tabla a partir de otra (create table-select) ](solucionario/solucionario_80.md)

[81. Vistas (create view) ](solucionario/solucionario_81.md)

[82. Vistas (información) ](solucionario/solucionario_82.md)

[83. Vistas eliminar (drop view)](solucionario/solucionario_83.md)

[84. Vistas (modificar datos a través de ella) ](solucionario/solucionario_84.md)

[85. Vistas (with read only) ](solucionario/solucionario_85.md)

[86. Vistas modificar (create or replace view) ](solucionario/solucionario_86.md)

[87. Vistas (with check option) ](solucionario/solucionario_87.md)

[88. Vistas (otras consideraciones: force) ](solucionario/solucionario_88.md)

[89. Vistas materializadas (materialized view) ](solucionario/solucionario_89.md)

[90. Procedimientos almacenados ](solucionario/solucionario_90.md)

[91. Procedimientos Almacenados (crear- ejecutar) ](solucionario/solucionario_91.md)

[92. Procedimientos Almacenados (eliminar) ](solucionario/solucionario_92.md)

[93. Procedimientos almacenados (parámetros de entrada) ](solucionario/solucionario_93.md)

[94. Procedimientos almacenados (variables) ](solucionario/solucionario_94.md)

[95. Procedimientos Almacenados (informacion) ](solucionario/solucionario_95.md)

[96. Funciones ](solucionario/solucionario_96.md)

[97. Control de flujo (if) ](solucionario/solucionario_97.md)

[98. Control de flujo (case) ](solucionario/solucionario_98.md)

[99. Control de flujo (loop)](solucionario/solucionario_99.md)

[100. Control de flujo (for) ](solucionario/solucionario_100.md)

[101. Control de flujo (while loop) ](solucionario/solucionario_101.md)

[102. Disparador (trigger) ](solucionario/solucionario_102.md)

[103. Disparador (información) ](solucionario/solucionario_103.md)

[104. Disparador de inserción a nivel de sentencia ](solucionario/solucionario_104.md)

[105. Disparador de insercion a nivel de fila (insert trigger for each row) ](solucionario/solucionario_105.md)

