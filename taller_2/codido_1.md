## SESSION 03
## PRACTICAS DE CONSULTAS 
```sql

alter session set "_ORACLE_SCRIPT"=true;
CREATE USER ELVIS
IDENTIFIED BY ELVIS
DEFAULT TABLESPACE USERS
TEMPORARY TABLESPACE TEMP;
ALTER USER ELVIS QUOTA UNLIMITED ON USERS;
GRANT CREATE SESSION, CREATE VIEW TO ELVIS;
GRANT RESOURCE TO ELVIS;
ALTER USER ELVIS DEFAULT ROLE RESOURCE;

SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE = 'ELVIS';

SELECT * FROM DBA_ROLE_PRIVS
WHERE GRANTEE = 'ELVIS';

SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE = 'RESOURCE';

SELECT * FROM DBA_SYS_PRIVS
WHERE GRANTEE = 'CONNECT';

CREATE USER HR                           -- Se le asigna un nombre de usuario
IDENTIFIED BY HR                         -- Se le asigna una contraseña (No es necesario usar doble comillas para hacerla case sensitive)
DEFAULT TABLESPACE USERS                 -- En Oracle XE se usa el tablespace USERS para almacenar los objetos de los usuarios
TEMPORARY TABLESPACE TEMP;               -- Asignar un tablespace temporal para los datos temporales de la sesión del usuario
ALTER USER HR QUOTA UNLIMITED ON USERS ;  -- Asignar una couta de almacenamiento en el tablespace USERS
GRANT CREATE SESSION TO HR;              -- Privilegio que permite que el usuario se pueda conectar a la BD (Iniciar Sesión)
GRANT CREATE VIEW TO HR;                 -- Privilegio de sistema que permite crear vistas (necesario para ejecutar script hr.sql)
GRANT RESOURCE TO HR;                    -- Asignar el rol "RESOURCE" (permite crear TABLE, SEQUENCE, TRIGGER, PROCEDURE, etc.)
ALTER USER HR DEFAULT ROLE RESOURCE;  

ALTER USER HR IDENTIFIED BY HR ACCOUNT UNLOCK;