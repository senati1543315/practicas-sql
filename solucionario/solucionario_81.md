[81. Vistas (create view) ](solucionario/solucionario_81.md)
# Solucionario.
# Vistas (create view).

## Ejercicios propuestos
# Un club dicta cursos de distintos deportes. Almacena la información en varias tablas. El director no quiere que los empleados de administración conozcan la estructura de las tablas ni algunos datos de los profesores y socios, por ello se crean vistas a las cuales tendrán acceso.
# Elimine las tablas y créelas nuevamente:
```sql
drop table inscriptos;
drop table cursos;
drop table socios;
drop table profesores;

create table socios(
    documento char(8) not null,
    nombre varchar2(40),
    domicilio varchar2(30),
    primary key (documento)
);

create table profesores(
    documento char(8) not null,
    nombre varchar2(40),
    domicilio varchar2(30),
    primary key (documento)
);

create table cursos(
    numero number(2),
    deporte varchar2(20),
    dia varchar2(15),
    documentoprofesor char(8),
    constraint CK_inscriptos_dia
    check (dia in('lunes','martes','miercoles','jueves','viernes','sabado')),
    constraint FK_documentoprofesor
    foreign key (documentoprofesor)
    references profesores(documento),
    primary key (numero)
);

create table inscriptos(
    documentosocio char(8) not null,
    numero number(2) not null,
    matricula char(1),
    constraint CK_inscriptos_matricula check (matricula in('s','n')),
    constraint FK_documentosocio
    foreign key (documentosocio)
    references socios(documento),
    constraint FK_numerocurso
    foreign key (numero)
    references cursos(numero),
    primary key (documentosocio,numero)
); 

```
# Ingrese algunos registros para todas las tablas:
```sql
insert into socios values('30000000','Fabian Fuentes','Caseros 987');
insert into socios values('31111111','Gaston Garcia','Guemes 65');
insert into socios values('32222222','Hector Huerta','Sucre 534');
insert into socios values('33333333','Ines Irala','Bulnes 345');
insert into profesores values('22222222','Ana Acosta','Avellaneda 231');
insert into profesores values('23333333','Carlos Caseres','Colon 245');
insert into profesores values('24444444','Daniel Duarte','Sarmiento 987');
insert into profesores values('25555555','Esteban Lopez','Sucre 1204');
insert into cursos values(1,'tenis','lunes','22222222');
insert into cursos values(2,'tenis','martes','22222222');
insert into cursos values(3,'natacion','miercoles','22222222');
insert into cursos values(4,'natacion','jueves','23333333');
insert into cursos values(5,'natacion','viernes','23333333');
insert into cursos values(6,'futbol','sabado','24444444');
insert into cursos values(7,'futbol','lunes','24444444');
insert into cursos values(8,'basquet','martes','24444444');
insert into inscriptos values('30000000',1,'s');
insert into inscriptos values('30000000',3,'n');
insert into inscriptos values('30000000',6,null);
insert into inscriptos values('31111111',1,'s');
insert into inscriptos values('31111111',4,'s');
insert into inscriptos values('32222222',8,'s');

```
# Elimine la vista "vista_club":
```sql
drop view vista_club;
```
# Cree una vista en la que aparezca el nombre del socio, el deporte, el día, el nombre del profesor y el estado de la matrícula (deben incluirse los socios que no están inscriptos en ningún deporte, los cursos para los cuales no hay inscriptos y los profesores que no tienen designado deporte también)
```sql
CREATE VIEW vista_club AS
SELECT s.nombre AS "Nombre Socio", c.deporte, c.dia, p.nombre AS "Nombre Profesor", i.matricula AS "Estado Matrícula"
FROM socios s
FULL OUTER JOIN inscriptos i ON s.documento = i.documentosocio
FULL OUTER JOIN cursos c ON i.numero = c.numero
FULL OUTER JOIN profesores p ON c.documentoprofesor = p.documento;

```
# Muestre la información contenida en la vista (11 registros)
```sql
SELECT * FROM vista_club;
```
# Realice una consulta a la vista donde muestre la cantidad de socios inscriptos en cada deporte (agrupe por deporte y día) ordenados por cantidad
```sql
SELECT deporte, dia, COUNT(*) AS "Cantidad de Inscriptos"
FROM vista_club
WHERE "Estado Matrícula" = 's'
GROUP BY deporte, dia
ORDER BY COUNT(*) DESC;

```
# Muestre (consultando la vista) los cursos (deporte y día) para los cuales no hay inscriptos (3 registros)
```sql
SELECT deporte, dia
FROM vista_club
WHERE "Estado Matrícula" IS NULL;

```
# Muestre los nombres de los socios que no se han inscripto en ningún curso (consultando la vista) (1 registro)
```sql
SELECT "Nombre Socio"
FROM vista_club
WHERE "Nombre Socio" IS NOT NULL AND deporte IS NULL;

```
# Muestre (consultando la vista) los profesores que no tienen asignado ningún deporte aún (1 registro)
```sql
SELECT "Nombre Profesor"
FROM vista_club
WHERE "Nombre Profesor" IS NOT NULL AND deporte IS NULL;

```
# Muestre (consultando la vista) el nombre de los socios que deben matrículas (1 registro)
```sql
SELECT "Nombre Socio"
FROM vista_club
WHERE "Estado Matrícula" = 'n';

```
# Consulte la vista y muestre los nombres de los profesores y los días en que asisten al club para dictar sus clases (9 registros)
```sql
SELECT DISTINCT "Nombre Profesor", dia
FROM vista_club
WHERE "Nombre Profesor" IS NOT NULL;

```
#  Muestre la misma información anterior pero ordenada por día
```sql
SELECT DISTINCT "Nombre Profesor", dia
FROM vista_club
WHERE "Nombre Profesor" IS NOT NULL
ORDER BY dia;

```
#  Muestre todos los socios que son compañeros en tenis los lunes (2 registros)
```sql
SELECT "Nombre Socio"
FROM vista_club
WHERE deporte = 'tenis' AND dia = 'lunes';

```
# Intente crear una vista denominada "vista_inscriptos" que muestre la cantidad de inscriptos por curso, incluyendo el número del curso, el nombre del deporte y el día
```sql
CREATE VIEW vista_inscriptos AS
SELECT c.numero, c.deporte, c.dia, COUNT(i.documentosocio) AS "Cantidad Inscriptos"
FROM cursos c
LEFT JOIN inscriptos i ON c.numero = i.numero
GROUP BY c.numero, c.deporte, c.dia;

```
# Elimine la vista "vista_inscriptos" y créela para que muestre la cantidad de inscriptos por curso, incluyendo el número del curso, el nombre del deporte y el día
```sql
DROP VIEW vista_inscriptos;

CREATE VIEW vista_inscriptos AS
SELECT c.numero, c.deporte, c.dia, COUNT(i.documentosocio) AS "Cantidad Inscriptos"
FROM cursos c
LEFT JOIN inscriptos i ON c.numero = i.numero
GROUP BY c.numero, c.deporte, c.dia;

```
# Consulte la vista (9 registros)
```sql
SELECT * FROM vista_inscriptos;
```