[17. Valores por defecto ](solucionario/solucionario_17.md)
# Solucionario  
# Valores por defecto (default)

# EJERCICIO 01
## Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.
## Elimine la tabla:

```sql
drop table articulos;
```
## Cree la tabla:

```sql
create table articulos(
    codigo number(4),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(8,2),
    cantidad number(3) default 0,
    primary key (codigo)
);
```
## Ingrese algunos registros:

```sql
insert into articulos
values (101,'impresora','Epson Stylus C45',400.80,20);

insert into articulos
values (203,'impresora','Epson Stylus C85',500,30);

insert into articulos
values (205,'monitor','Samsung 14',800,10);

insert into articulos
values (300,'teclado','ingles Biswal',100,50);
```
## El comercio quiere aumentar los precios de todos sus artículos en un 15%. Actualice todos los precios empleando operadores aritméticos.

```sql
UPDATE articulos SET precio = precio * 1.15;
```
## 05 Vea el resultado.

```sql
select * from articulos;
```
## Muestre todos los artículos, concatenando el nombre y la descripción de cada uno de ellos separados por coma.

```sql
SELECT nombre, ', ', descripcion FROM articulos;
```
## Reste a la cantidad de todas las impresoras, el valor 5, empleando el operador aritmético menos ("-")

```sql
UPDATE articulos SET cantidad = cantidad - 5;
```
## Recupere todos los datos de las impresoras para verificar que la actualización se realizó.

```sql
select * from articulos where nombre = 'impresora';
```
## Muestre todos los artículos concatenado los campos para que aparezcan de la siguiente manera "Cod. 101: impresora Epson Stylus C45 $460,92 (15)"

```sql
SELECT 'Cod. ', Codigo, ': ', Nombre, ' $', Precio, ' (', Cantidad, ')' FROM Articulos;
```