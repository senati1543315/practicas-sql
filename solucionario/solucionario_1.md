[1. crear tablas(create tabla _ all_tables - drop table)](solucionario/solucionario_1.md)
# Solucionario  
# Crear tablas (create table - describe - all_tables - drop table)

## Ejercicio 01 creamos la tabla "agenda"
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```
## Ingresamos los datos

```sql

INSERT INTO agenda (apellido, nombre, domicilio, telefono)
VALUES ('Suni', 'Yuguiro', 'Jr juli 106', '978696107');
INSERT INTO agenda (apellido, nombre, domicilio, telefono)
VALUES ('Zurita', 'Danner', 'Jr salcedo 111', '981802016');
INSERT INTO agenda (apellido, nombre, domicilio, telefono)
VALUES ('Pari', 'Yakelin', 'Jr los incas', '951120303');
INSERT INTO agenda (apellido, nombre, domicilio, telefono)
VALUES ('flores', 'Joselin', 'Jr laykakota', '951120505');

```

# Ejercicio 02

## Creamos la tabla " biblioteca"
```sql
CREATE TABLE biblioteca (
  id INT,
  titulo VARCHAR2(100),
  autor VARCHAR2(100),
  editorial VARCHAR2(100)
);
```
### Creamos la tabla libros
```sql
CREATE TABLE libros (
  titulo VARCHAR2(20),
  autor VARCHAR2(30),
  editorial VARCHAR2(15)
);
```
#### Visualizamos la   estructura de la tabla libros
```sh
DESCRIBE libros;

Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 
```

##### Eliminamos la tabla
```sh
DROP TABLE libros;

Table LIBROS borrado.
```