[39.crear tablas 39 ](solucionario/solucionario_39.md)
## Practicas de laboratorio
## ejercio01
## 1
```sql
drop table empleados;
```
## 2
```sql
create table empleados (
  documento char(8),
  nombre varchar2(30),
  cantidadhijos number(2),
  seccion varchar2(20),
  sueldo number(6,2) default -1
 );
```
## 3
```sql
alter table empleados
 add constraint CK_empleados_sueldo_positivo
 check (sueldo>0);


```
## 4
```sql

 insert into empleados values ('33555888','Juan Pereyra',1,'Sistemas',default);
```
## 5
```sql

 insert into empleados values ('22222222','Alberto Lopez',1,'Sistemas',1000);
 insert into empleados values ('33333333','Beatriz Garcia',2,'Administracion',3000);
 insert into empleados values ('34444444','Carlos Caseres',0,'Contaduría',6000);
```
## 6
```sql
 alter table empleados
 add constraint CK_empleados_sueldo_maximo
 check (sueldo<=5000);
```
## 7
```sql
 delete from empleados where sueldo=6000;
```

## 8
```sql
 alter table empleados
 add constraint CK_empleados_seccion_lista
 check (seccion in ('Sistemas','Administracion','Contaduria'));
```
## 9
```sql

 insert into empleados
  values ('31111222','Roberto Sanchez',1,null,1500);

```
## 10
```sql

 alter table empleados
 add constraint CK_cantidadhijos
 check (cantidadhijos between 0 and 15);
```
## 11
```sql

 select *from user_constraints where table_name='EMPLEADOS';
```
## 12
```sql
 insert into empleados
  values ('24444444','Carlos Fuentes',2,'Administracion',-1500);

```
## 13
```sql

 update empleados set cantidadhijos=21 where documento='22222222';
```
## 14
```sql
 update empleados set seccion='Recursos' where documento='22222222';
```
## 15
```sql
 update empleados set seccion='Recursos' where documento='22222222';
```
## 16
```sql
 insert into empleados values(null,'Federico Torres',1,'Sistemas',1800);
```
## 17
```sql

 alter table empleados
 add constraint PK_empleados_documento
 primary key (documento);
```
## 18
```sql
 delete from empleados where nombre='Federico Torres';
 alter table empleados
 add constraint PK_empleados_documento
 primary key (documento);

```
## 19
```sql
 select constraint_name, constraint_type, search_condition
  from user_constraints
  where table_name='EMPLEADOS';
```
## 20
```sql
 select constraint_name
  from user_cons_columns
  where table_name='EMPLEADOS' and
  column_name='SUELDO';

```
## ejercicios propuestos 2
## 1
```sql
 alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```
## 2
```sql
drop table vehiculos;

 create table vehiculos(
  numero number(5),
  patente char(6),
  tipo char(4),
  fechahoraentrada date,
  fechahorasalida date
 );
```

## 3
```sql
 insert into vehiculos values(1,'AIC124','auto','17/01/2017 8:05','17/01/2017 12:30');
 insert into vehiculos values(2,'CAA258','auto','17/01/2017 8:10',null);
 insert into vehiculos values(3,'DSE367','moto','17/01/2017 8:30','17/01/2017 18:00');

```

## 4
```sql
 alter table vehiculos
 add constraint CK_vehiculos_tipo_valores
 check (tipo in ('auto','moto'));
```

## 5
```sql
update vehiculos set tipo='bici' where patente='AIC124';
```

## 6
```sql
insert into vehiculos values(4,'SDF134',null,null,null);
```

## 7
```sql
 alter table vehiculos
  add constraint CK_vehiculos_entradasalida
  check (fechahoraentrada<=fechahorasalida);
```

## 8
```sql
 update vehiculos set fechahorasalida='17/01/2017 7:30'
  where patente='CAA258';
```

## 9
```sql
 select *from user_constraints where table_name='VEHICULOS';
```

## 10
```sql
 insert into vehiculos values(8,'DFR156','moto',null,default);
```
## 11
```sql
 select *from vehiculos;
```
## 12
```sql
 select *from user_cons_columns where table_name='VEHICULOS';
```


