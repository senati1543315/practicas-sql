[14. Tipos de datos alfanuméricos ](solucionario/solucionario_14.md)
# Solucionario  
# Tipos de datos alfanuméricos

# EJERCICIO 01
## Una concesionaria de autos vende autos usados y almacena los datos de los autos en una tabla llamada "autos".
## 0Elimine la tabla "autos"

```sql
drop table autos;
```

```sh
Error starting at line : 1 in command -
drop table autos
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
## Cree la tabla eligiendo el tipo de dato adecuado para cada campo, estableciendo el campo "patente" como clave primaria:

```sql
create table autos(
    patente char(6),
    marca varchar2(20),
    modelo char(4),
    precio number(8,2),
    primary key (patente)
);
```

```sh
Table AUTOS created.
```
## Ingrese los siguientes registros:

```sql
insert into autos (patente,marca,modelo,precio) values('ABC123','Fiat 128','1970',15000);
insert into autos (patente,marca,modelo,precio) values('BCD456','Renault 11','1990',40000);
insert into autos (patente,marca,modelo,precio) values('CDE789','Peugeot 505','1990',80000);
insert into autos (patente,marca,modelo,precio) values('DEF012','Renault Megane','1998',95000);
```

```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.

```
## Ingrese un registro omitiendo las comillas en el valor de "modelo" Oracle convierte el valor a cadena.

```sql
insert into autos (patente,marca,modelo,precio) values('DEF02','Relt Megane',1998,95000);
```

```sh
Error starting at line : 1 in command -
insert into autos (patente,marca,modelo,precio) values('DEF02','Relt Megane',1998,95000)
Error report -
ORA-00001: unique constraint (YUGUIRO.SYS_C008400) violated
```
## Vea cómo se almacenó.

```sql
select * from autos;
```

```sh
PATENT MARCA                MODE     PRECIO
------ -------------------- ---- ----------
ABC123 Fiat 128             1970      15000
BCD456 Renault 11           1990      40000
CDE789 Peugeot 505          1990      80000
DEF012 Renault Megane       1998      95000
DEF02  Relt Megane          1998      95000
```
## Seleccione todos los autos modelo "1990"

```sql
SELECT * FROM autos WHERE modelo = '1990';
```

```sh
PATENT MARCA                MODE     PRECIO
------ -------------------- ---- ----------
BCD456 Renault 11           1990      40000
CDE789 Peugeot 505          1990      80000
```
## Intente ingresar un registro con un valor de patente de 7 caracteres

```sql
insert into autos (patente,marca,modelo,precio) values('DEFSCAH','Relt Megane',1998,95000);
```

```sh
Error starting at line : 1 in command -
insert into autos (patente,marca,modelo,precio) values('DEFSCAH','Relt Megane',1998,95000)
Error at Command Line : 1 Column : 56
Error report -
SQL Error: ORA-12899: value too large for column "YUGUIRO"."AUTOS"."PATENTE" (actual: 7, maximum: 6)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).

```
## 08 Intente ingresar un registro con valor de patente repetida.

```sql
insert into autos (patente,marca,modelo,precio) values('DEFSCAH','Relt Megane',1998,95000);
```

```sh
Error starting at line : 1 in command -
insert into autos (patente,marca,modelo,precio) values('DEFSCAH','Relt Megane',1998,95000)
Error at Command Line : 1 Column : 56
Error report -
SQL Error: ORA-12899: value too large for column "YUGUIRO"."AUTOS"."PATENTE" (actual: 7, maximum: 6)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).
```
## EJERCICIO 02
# Una empresa almacena los datos de sus clientes en una tabla llamada "clientes".
## Elimine la tabla "clientes"

```sql
drop table clientes;
```

```sh
Error starting at line : 1 in command -
drop table clientes
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
## Créela eligiendo el tipo de dato más adecuado para cada campo:
```sql
create table clientes(
    documento char(8) not null,
    apellido varchar2(20),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2 (11)
);
```

```sh
Table CLIENTES created.
```
## Analice la definición de los campos. Se utiliza char(8) para el documento porque siempre constará de 8 caracteres. Para el número telefónico se usar "varchar2" y no un tipo numérico porque si bien es un número, con él no se realizarán operaciones matemáticas.
create table clientes(
    documento char(8) not null,
    apellido varchar2(20),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2 (11)
);
## Ingrese algunos registros:

```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('22333444','Perez','Juan','Sarmiento 980','4223344');
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('23444555','Perez','Ana','Colon 234',null);
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Garcia','Luciana','Caseros 634',null);

```

```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## Intente ingresar un registro con más caracteres que los permitidos para el campo "telefono"

```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Garcia','Luciana','Caseros 634','65465465465478');
```

```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## Intente ingresar un registro con más caracteres que los permitidos para el campo "documento"

```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Garcia','Luciana','Caseros 634','65465465465478');
```

```sh
Error starting at line : 1 in command -
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Garcia','Luciana','Caseros 634','65465465465478')
Error at Command Line : 1 Column : 120
Error report -
SQL Error: ORA-12899: value too large for column "YUGUIRO"."CLIENTES"."TELEFONO" (actual: 14, maximum: 11)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).
```
## Intente ingresar un registro omitiendo las comillas en el campo "apellido"

```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555',Garcia,'Luciana','Caseros 634','65465465465478');
```

```sh
Error starting at line : 1 in command -
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555',Garcia,'Luciana','Caseros 634','65465465465478')
Error at Command Line : 1 Column : 87
Error report -
SQL Error: ORA-00984: column not allowed here
00984. 00000 -  "column not allowed here"
*Cause:    
*Action:
```
## Seleccione todos los clientes de apellido "Perez" (2 registros)

```sql
select * from clientes where apellido='Perez';
```

```sh
DOCUMENT APELLIDO             NOMBRE               DOMICILIO                      TELEFONO   
-------- -------------------- -------------------- ------------------------------ -----------
22333444 Perez                Juan                 Sarmiento 980                  4223344    
23444555 Perez                Ana                  Colon 234                                 
```