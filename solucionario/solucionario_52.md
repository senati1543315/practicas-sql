[52. Autocombinación ](solucionario/solucionario_52.md)

# Solucionario
# Autocombinación.

## Ejercicios propuestos
# Una agencia matrimonial almacena la información de sus clientes en una tabla llamada "clientes".
## Elimine la tabla y créela:

```sql
drop table clientes;

create table clientes(
    nombre varchar2(30),
    sexo char(1),--'f'=femenino, 'm'=masculino
    edad number(2),
    domicilio varchar2(30)
);

```
## Ingrese los siguientes registros:

```sql
insert into clientes values('Maria Lopez','f',45,'Colon 123');
insert into clientes values('Liliana Garcia','f',35,'Sucre 456');
insert into clientes values('Susana Lopez','f',41,'Avellaneda 98');
insert into clientes values('Juan Torres','m',44,'Sarmiento 755');
insert into clientes values('Marcelo Oliva','m',56,'San Martin 874');
insert into clientes values('Federico Pereyra','m',38,'Colon 234');
insert into clientes values('Juan Garcia','m',50,'Peru 333');
```
## La agencia necesita la combinación de todas las personas de sexo femenino con las de sexo masculino. Use un "cross join" (12 filas)

```sql
SELECT f.nombre AS mujer, f.domicilio AS domicilio_mujer, f.edad AS edad_mujer,
       m.nombre AS varon, m.domicilio AS domicilio_varon, m.edad AS edad_varon
FROM clientes f
CROSS JOIN clientes m
WHERE f.sexo = 'f' AND m.sexo = 'm';
```
## Obtenga la misma salida anterior pero realizando un "join"

```sql
SELECT f.nombre AS mujer, f.domicilio AS domicilio_mujer, f.edad AS edad_mujer,
       m.nombre AS varon, m.domicilio AS domicilio_varon, m.edad AS edad_varon
FROM clientes f
JOIN clientes m ON f.sexo = 'f' AND m.sexo = 'm';
```
## Realice la misma autocombinación que el punto 3 pero agregue la condición que las parejas no tengan una diferencia superior a 5 años (5 filas)

```sql
SELECT f.nombre AS mujer, f.domicilio AS domicilio_mujer, f.edad AS edad_mujer,
       m.nombre AS varon, m.domicilio AS domicilio_varon, m.edad AS edad_varon
FROM clientes f
JOIN clientes m ON f.sexo = 'f' AND m.sexo = 'm' AND ABS(f.edad - m.edad) <= 5;
```