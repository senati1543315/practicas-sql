[7. Borrar registros  ](solucionario/solucionario_7.md)
# Solucionario  
# Borrar registros (delete).

# EJERCICIO 01.
## Trabaje con la tabla "agenda" que registra la información referente a sus amigos. 

## Elimine la tabla.

```sql
drop table agenda;
```

```sh
Table AGENDA dropped.
```
## Cree la tabla con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```

```sh
Table AGENDA created.
```
## Ingrese los siguientes registros (insert into):

```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Alvarez','Alberto','Colon 123','4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Salas','Susana','Gral. Paz 1234','4123456');
```

```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## Elimine el registro cuyo nombre sea "Juan" (1 registro)

```sql
DELETE FROM agenda WHERE nombre = 'Juan';
```

```sh
1 row deleted.
```
## Elimine los registros cuyo número telefónico sea igual a "4545454" (2 registros)

```sql
DELETE FROM agenda WHERE telefono = '4545454';
```

```sh
2 rows deleted.
```
## Elimine todos los registros (2 registros)

```sql
DELETE FROM agenda;
```

```sh
2 rows deleted.
```
# EJERCICIO 02.
## Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.
## Elimine "articulos"

```sql
drop table articulos;
```

```sh
Table ARTICULOS dropped.
```
## Cree la tabla, con la siguiente estructura:

```sql
create table articulos(
    codigo number(4,0),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2),
    cantidad number(3)
);

```

```sh
Table ARTICULOS created.
```
## Vea la estructura de la tabla.

```sql
select * from articulos;
```

```sh
codigo nombre descripcion precio cantidad
```
## Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);
```

```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.

```
## Elimine los artículos cuyo precio sea mayor o igual a 500 (2 registros)

```sql
delete from articulos where precio >= 500;
```

```sh
2 rows deleted.
```
## Elimine todas las impresoras (1 registro)

```sql
delete from articulos where nombre='impresora';
```

```sh
1 rows deleted.
```
## Elimine todos los artículos cuyo código sea diferente a 4 (1 registro)

```sql
delete from articulos;
```

```sh
1 rows deleted.
```