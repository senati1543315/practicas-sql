[98. Control de flujo (case) ](solucionario/solucionario_98.md)
# Solucionario.
# Control de flujo (case)

## Ejercicios propuestos

Un negocio almacena los datos de sus productos en una tabla denominada "productos". Dicha tabla contiene el código de producto, el precio, el stock mínimo que se necesita (cantidad mínima requerida antes de comprar más) y el stock actual (cantidad disponible en depósito). Si el stock actual es cero, es urgente reponer tal producto; menor al stock mínimo requerido, es necesario reponer tal producto; si el stock actual es igual o supera el stock minimo, está en estado normal.

1. Elimine la tabla "productos":

```sql
drop table productos;
```

2. Cree la tabla con la siguiente estructura:

```sql
 create table productos(
  codigo number(5),
  precio number(6,2),
  stockminimo number(4),
  stockactual number(4)
 );
```

3. Ingrese algunos registros:

```sql
insert into productos values(100,10,100,200);
insert into productos values(102,15,200,500);
insert into productos values(130,8,100,0);
insert into productos values(240,23,100,20);
insert into productos values(356,12,100,250);
insert into productos values(360,7,100,100);
insert into productos values(400,18,150,100);
```

4. Cree una función que reciba dos valores numéricos correspondientes a ambos stosks. Debe comparar ambos stocks y retornar una cadena de caracteres indicando el estado de cada producto, si stock actual es:

 - cero: "faltante",
 - menor al stock mínimo: "reponer",
 - igual o superior al stock mínimo: "normal".
5. Realice un "select" mostrando el código del producto, ambos stocks y, empleando la función creada anteriormente, una columna que muestre el estado del producto

6. Realice la misma función que en el punto 4, pero esta vez empleando en la estructura condicional la sintaxis "if... elsif...end if"

7. Realice un "select" mostrando el código del producto, ambos stocks y, empleando la función creada anteriormente, una columna que muestre el estado del producto

8. Realice una función similar a las anteriores, pero esta vez, si el estado es "reponer" o "faltante", debe especificar la cantidad necesaria (valor necesario para llegar al stock mínimo)

7. Realice un "select" mostrando el código del producto, ambos stocks y, empleando la función creada anteriormente, una columna que muestre el estado del producto