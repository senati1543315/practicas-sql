[51. Combinaciones cruzadas (cross)](solucionario/solucionario_51.md
# Solucionario.
# Combinaciones cruzadas (cross)

## Ejercicios propuestos
# Una agencia matrimonial almacena la información de sus clientes de sexo femenino en una tabla llamada "mujeres" y en otra la de sus clientes de sexo masculino llamada "varones".
## 01 Elimine las tablas y créelas con las siguientes estructuras:

```sql
drop table mujeres;
drop table varones;

create table mujeres(
    nombre varchar2(30),
    domicilio varchar2(30),
    edad number(2)
);

create table varones(
    nombre varchar2(30),
    domicilio varchar2(30),
    edad number(2)
);
```
## Ingrese los siguientes registros:

```sql
insert into mujeres values('Maria Lopez','Colon 123',45);
insert into mujeres values('Liliana Garcia','Sucre 456',35);
insert into mujeres values('Susana Lopez','Avellaneda 98',41);
insert into varones values('Juan Torres','Sarmiento 755',44);
insert into varones values('Marcelo Oliva','San Martin 874',56);
insert into varones values('Federico Pereyra','Colon 234',38);
insert into varones values('Juan Garcia','Peru 333',50);
```
## La agencia necesita la combinación de todas las personas de sexo femenino con las de sexo masculino. Use un "cross join" (12 filas)

```sql
SELECT m.nombre AS mujer, m.domicilio AS domicilio_mujer, m.edad AS edad_mujer,
       v.nombre AS varon, v.domicilio AS domicilio_varon, v.edad AS edad_varon
FROM mujeres m
CROSS JOIN varones v;
```
## Realice la misma combinación pero considerando solamente las personas mayores de 40 años (6 filas)

```sql
SELECT m.nombre AS mujer, m.domicilio AS domicilio_mujer, m.edad AS edad_mujer,
       v.nombre AS varon, v.domicilio AS domicilio_varon, v.edad AS edad_varon
FROM mujeres m
CROSS JOIN varones v
WHERE m.edad > 40 AND v.edad > 40;
```
## Forme las parejas pero teniendo en cuenta que no tengan una diferencia superior a 10 años (8 filas)

```sql
SELECT m.nombre AS mujer, m.domicilio AS domicilio_mujer, m.edad AS edad_mujer,
       v.nombre AS varon, v.domicilio AS domicilio_varon, v.edad AS edad_varon
FROM mujeres m
JOIN varones v ON ABS(m.edad - v.edad) <= 10;
```