[42. Restricciones: eliminación ](solucionario/solucionario_42.md)
## Ejercicios propuestos
## Primer problema:
## Una playa de estacionamiento almacena cada día los datos ## de los vehículos que ingresan en la tabla llamada "vehiculos".
## Setee el formato de "date" para que nos muestre hora y minutos:
## 1
```sql
alter SESSION SET NLS_DATE_FORMAT = 'HH24:MI';
```
## 2
```sql
drop table vehiculos;

create table vehiculos(
    patente char(6) not null,
    tipo char(1),--'a'=auto, 'm'=moto
    horallegada date not null,
    horasalida date
);

```
## 3
```sql
alter table vehiculos
add constraint CK_vehiculos_tipo
check (tipo in ('a','m'));

```
## 4
```sql
 alter table vehiculos
 add constraint PK_vehiculos
 primary key(patente,horallegada);

```
## 5
```sql
insert into vehiculos values('SDR456','a','10:10',null);
```
## 6
```sql

 insert into vehiculos values('SDR456','m','10:10',null);
```
## 7
```sql
 insert into vehiculos values('SDR456','m','12:20',null);
```
## 8
```sql
insert into vehiculos values('SDR111','m','10:10',null);
```
## 9
```sql
 select constraint_name, constraint_type, search_condition from user_constraints
  where table_name='VEHICULOS';

```
## 10
```sql
alter table vehiculos
  drop constraint PK_vehiculos;
```
## 11
```sql
select constraint_name, constraint_type, search_condition from user_constraints
  where table_name='VEHICULOS';
```
## 12
```sql

 select constraint_name, constraint_type, search_condition from user_constraints
  where table_name='VEHICULOS';
```
## 13
```sql
 alter table vehiculos
 add constraint PK_vehiculos
 primary key(patente,horallegada);

```
## 14
```sql
 alter table vehiculos
  drop constraint CK_vehiculos_tipo;

```
## 15
```sql

 alter table vehiculos
 add constraint CK_vehiculos_tipo
 check (tipo in ('a','m','c'));
```
## 16
```sql

 select search_condition from user_constraints
  where table_name='VEHICULOS' and
  constraint_name='CK_VEHICULOS_TIPO';
```

