
[6. Operadores relacionales ](solucionario/solucionario_6.md)
# Solucionario  
# Operadores relacionales

# EJERCICIO 01.
## Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

## Elimine "articulos"

```sql
drop table articulos;
```

```sh
Table ARTICULOS dropped.
```
## Cree la tabla, con la siguiente estructura:

```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(6,2),
    cantidad number(3)
);
```

```sh
Table ARTICULOS created.
```
## Vea la estructura de la tabla.

```sql
describe articulos;
```

```sh
Name        Null? Type         
----------- ----- ------------ 
CODIGO            NUMBER(5)    
NOMBRE            VARCHAR2(20) 
DESCRIPCION       VARCHAR2(30) 
PRECIO            NUMBER(6,2)  
CANTIDAD          NUMBER(3)  
```
## Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);
```

```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```