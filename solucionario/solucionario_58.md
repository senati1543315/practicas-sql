[58. Restricciones foreign key en la misma tabla ](solucionario/solucionario_58.md)
# Solucionario.
# Restricciones foreign key en la misma tabla

## Ejercicios propuestos
# Una empresa registra los datos de sus clientes en una tabla llamada "clientes". Dicha tabla contiene un campo que hace referencia al cliente que lo recomendó denominado "referenciadopor". Si un cliente no ha sido referenciado por ningún otro cliente, tal campo almacena "null"

## Elimine la tabla y créela:

```sql
 drop table clientes;
 
create table clientes(
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    referenciadopor number(5),
    primary key(codigo)
);

```
## Ingresamos algunos registros:

```sql
insert into clientes values (50,'Juan Perez','Sucre 123','Cordoba',null);
insert into clientes values(90,'Marta Juarez','Colon 345','Carlos Paz',null);
insert into clientes values(110,'Fabian Torres','San Martin 987','Cordoba',50);
insert into clientes values(125,'Susana Garcia','Colon 122','Carlos Paz',90);
insert into clientes values(140,'Ana Herrero','Colon 890','Carlos Paz',9);

```
## Intente agregar una restricción "foreign key" para evitar que en el campo "referenciadopor" se ingrese un valor de código de cliente que no exista. No se permite porque existe un registro que no cumple con la restricción que se intenta establecer.

```sql
ALTER TABLE clientes
ADD CONSTRAINT fk_referenciadopor
FOREIGN KEY (referenciadopor)
REFERENCES clientes(codigo);
```
## Cambie el valor inválido de "referenciadopor" del registro que viola la restricción por uno válido:

```sql
update clientes set referenciadopor=90 where referenciadopor=9;
```
## Agregue la restricción "foreign key" que intentó agregar en el punto 3

```sql
ALTER TABLE clientes
ADD CONSTRAINT fk_referenciadopor
FOREIGN KEY (referenciadopor)
REFERENCES clientes(codigo);
```
## Vea la información referente a las restricciones de la tabla "clientes" La tabla tiene 2 restricciones

```sql
SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'CLIENTES';
``` 
## Intente agregar un registro que infrinja la restricción.

```sql
INSERT INTO clientes VALUES (200, 'Pedro Rodriguez', 'Rivadavia 456', 'Cordoba', 999);
```
## Intente modificar el código de un cliente que está referenciado en "referenciadopor"

```sql
UPDATE clientes SET codigo = 51 WHERE codigo = 50;
```
## Intente eliminar un cliente que sea referenciado por otro en "referenciadopor"

```sql
DELETE FROM clientes WHERE codigo = 90;
```
## Cambie el valor de código de un cliente que no referenció a nadie.

```sql
UPDATE clientes SET codigo = 55 WHERE codigo = 50;
```
## Elimine un cliente que no haya referenciado a otros.

```sql
DELETE FROM clientes WHERE codigo = 55;
```