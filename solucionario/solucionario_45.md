[45. Indices (eliminar)](solucionario/solucionario_45.md)
## ejercicios
## 1
```sql
 drop table alumnos;
 create table alumnos(
  legajo char(5) not null,
  documento char(8) not null,
  nombre varchar(30),
  curso char(1) not null,
  materia varchar2(20) not null,
  notafinal number(4,2)
 );

```
## 2
```sql
create index I_alumnos_nombre
  on alumnos(nombre);
```
## 3
```sql
alter table alumnos
  add constraint PK_alumnos_legajo
  primary key (legajo);
```
## 4
```sql
 select constraint_name, constraint_type, index_name
  from user_constraints
  where table_name='ALUMNOS';
```
## 5
```sql
select index_name,uniqueness
  from user_indexes
  where table_name='ALUMNOS';
```
## 6
```sql

 drop index PK_alumnos_legajo;
```
## 7
```sql
create unique index I_alumnos_documento
  on alumnos(documento);
```
## 8
```sql
 alter table alumnos
  add constraints UQ_alumnos_documento
  unique (documento);

```
## 9
```sql
select constraint_name, constraint_type, index_name
  from user_constraints
  where table_name='ALUMNOS';
```
## 10
```sql
 drop index I_alumnos_documento;
```
## 11
```sql
 alter table alumnos
  drop constraint UQ_ALUMNOS_DOCUMENTO;
```
## 12
```sql
 select index_name,uniqueness
  from user_indexes
  where table_name='ALUMNOS';
```
## 13
```sql

 drop index I_alumnos_documento;
```
## 14
```sql
 drop index I_alumnos_nombre;
```
## 15
```sql

 alter table alumnos
 drop constraint PK_ALUMNOS_LEGAJO;

```
## 16
```sql
 select index_name,uniqueness
  from user_indexes
  where table_name='ALUMNOS';

```
## 17
```sql

 create index I_alumnos_cursomateria
  on alumnos(curso,materia);

```
## 18
```sql
 select index_name,uniqueness
  from user_indexes
  where table_name='ALUMNOS';
```
## 19
```sql

 drop table alumnos;
```
## 20
```sql
 select index_name,uniqueness
  from user_indexes
  where table_name='ALUMNOS';
```
