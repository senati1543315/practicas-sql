# [2. ingresar registros ](solucionario/solucionario_2.md)
# Ejercicio 01
##### Trabaje con la tabla "agenda" que almacena información de sus amigos.
#### Elimine la tabla "agenda"
```sql
drop table agenda;

salida del codigo:
Table AGENDA borrado.

```

#### Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11)
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
salida del codigo:
Table AGENDA creado.

```

#### Visualice las tablas existentes para verificar la creación de "agenda" (all_tables)
```sql
select * from all_tables


salida del codigo:
TEDDY2  AGENDA  SENATI...
```

#### Visualice la estructura de la tabla "agenda" (describe)
```sql
DESCRIBE agenda;

salida del codigo:
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11)
```

#### Ingrese los siguientes registros:
##### insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567');
##### insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');
```sql
insert into agenda (apellido, nombre, domicilio, telefono) values ('moreno','alberto','colon 123','4234567');
insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');

salida del codigo:
1 fila insertadas.
1 fila insertadas.
```
#### Seleccione todos los registros de la tabla.
```sql
select * from agenda;

salida del codigo:
moreno  alberto colon 123       4234567
Torres  Juan    Avellaneda 135  4458787
```

#### Elimine la tabla "agenda"
```sql
drop table agenda;

salida del codigo:
Table AGENDA borrado.

```

#### Intente eliminar la tabla nuevamente (aparece un mensaje de error)
```sql
drop table agenda;

salida del codigo:

Error que empieza en la línea: 8 del comando :
drop table agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
# Ejercicio 02
##### Trabaje con la tabla "libros" que almacena los datos de los libros de su propia biblioteca.
#### Elimine la tabla "libros"
```sql
drop table libros;

salida del codigo:
Table LIBROS borrado.

```
#### Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15)
```sql
create table libros(
    titulo varchar2(20),
    autor varchar2(30),
    editorial varchar2(15)
);
salida del codigo:
Table LIBROS creado.
```

#### Visualice las tablas existentes.
```sql
select * from all_tables

salida del codigo:
ELVIS2  LIBROS  SENATI...
ELVIS2  AGENDA  SENATI...
```

#### Visualice la estructura de la tabla "libros"
```sql
DESCRIBE libros;

salida del codigo:
Table LIBROS creado.

Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 
```

##### Muestra los campos y los tipos de datos de la tabla "libros".
#### Ingrese los siguientes registros:

```sql
insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');

salida del codigo:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```

#### Muestre todos los registros (select) de "libros"

```sql
select * from libros;

salida del codigo:
El aleph          Borges            Planeta
Martin Fierro     Jose Hernandez    Emece
Aprenda PHP       Mario Molina      Emece
```
