# [3.tipo de datos ](solucionario/solucionario_3.md)
# Ejercicio 01
##### Un videoclub que alquila películas en video almacena la información de sus películas en una tabla llamada "peliculas"; para cada película necesita los siguientes datos:

#### Elimine la tabla "peliculas" si ya existe.
```sql
drop table peliculas;

salida del codigo:
Error que empieza en la línea: 8 del comando :
drop table peliculas
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```

#### Cree la tabla eligiendo el tipo de dato adecuado para cada campo.
```sql
create table peliculas(
    nombre varchar2(20),
    actor varchar2(20),
    duracion number,
    cantidad number
);
salida del codigo:
Table PELICULAS creado.
```
###### Note que los campos "duracion" y "cantidad", que almacenarán valores sin decimales, fueron definidos de maneras diferentes, en el primero especificamos el valor 0 como cantidad de decimales, en el segundo no especificamos cantidad de decimales, es decir, por defecto, asume el valor 0.
#### Vea la estructura de la tabla.
```sql
describe pelicula;

salida del codigo:
Table PELICULAS creado.

Nombre   ¿Nulo? Tipo         
-------- ------ ------------ 
NOMBRE          VARCHAR2(20) 
ACTOR           VARCHAR2(20) 
DURACION        NUMBER       
CANTIDAD        NUMBER       
```
#### Ingrese los siguientes registros:
```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',128,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',130,2);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Julia Roberts',118,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',110,2);

salida del codigo:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.

```
#### Muestre todos los registros (4 registros)
```sql
select * from peliculas;

salida del codigo:
Mision imposible    Tom Cruise  128 3
Mision imposible 2  Tom Cruise  130 2
Mujer bonita      Julia Roberts 118 3
Elsa y Fred      China Zorrilla 110 2
```

#### Intente ingresar una película con valor de cantidad fuera del rango permitido:
```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Richard Gere',1200,10);
salida del codigo:

Error en la línea de comandos : 21 Columna : 41
Informe de error -
Error SQL: ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
01438. 00000 -  "value larger than specified precision allowed for this column"
*Cause:    When inserting or updating records, a numeric value was entered
           that exceeded the precision defined for the column.
*Action:   Enter a value that complies with the numeric column's precision,
           or use the MODIFY option with the ALTER TABLE command to expand
           the precision.

```
#### Ingrese un valor con decimales en un nuevo registro, en el campo "duracion":
```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Richard Gere',120.20,4);


salida del codigo: 
1 fila insertadas.
Mujer bonita    Richard Gere    120,2   4
```
#### Muestre todos los registros para ver cómo se almacenó el último registro ingresado.
```sql
select * from peliculas;

salida del codigo: 

Mujer bonita      Richard Gere    120,2 4
Mision imposible    Tom Cruise    128   3
Mision imposible 2  Tom Cruise    130   2
Mujer bonita       Julia Roberts  118   3
Elsa y Fred     China Zorrilla    110   2
Mujer bonita    Richard Gere     1200   10
```

#### Intente ingresar un nombre de película que supere los 20 caracteres.
```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('los mas rapidos y los mas furiosos','Richard Gere',120.20,4);

salida del codigo:

Error en la línea de comandos : 22 Columna : 67
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "TEDDY2"."PELICULAS"."NOMBRE" (real: 34, máximo: 20)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).
```
# Ejercicio 02
###### Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.
#### Elimine la tabla si existe.
```sql
drop table empleados;

salida del codigo:

Error que empieza en la línea: 8 del comando :
drop table empleados
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```

#### Cree la tabla eligiendo el tipo de dato adecuado para cada campo:
```sql
create table empleados(
    nombre varchar2(20),
    documento varchar2(8),
    sexo varchar2(1),
    domicilio varchar2(30),
    sueldobasico number(6,2)
);
salida del codigo:
Table EMPLEADOS creado.

```
#### Verifique que la tabla existe consultando
```sql
select * from all_tables

salida del codigo:
TEDDY2  EMPLEADOS   SENATI....
```


#### Vea la estructura de la tabla (5 campos)
```sql
DESCRIBE empleados;

salida del codigo:
Table EMPLEADOS creado.

Nombre       ¿Nulo? Tipo         
------------ ------ ------------ 
NOMBRE              VARCHAR2(20) 
DOCUMENTO           VARCHAR2(8)  
SEXO                VARCHAR2(1)  
DOMICILIO           VARCHAR2(30) 
SUELDOBASICO        NUMBER(6,2)  
```

#### Ingrese algunos registros:
```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22333444','m','Sarmiento 123',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24555666','f','Colon 134',650);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','m','Urquiza 479',800);

salida del codigo:
1 fila insertadas.
1 fila insertadas.
1 fila insertadas.
```

#### Seleccione todos los registros (3 registros)
```sql
select * from empleados;

salida del codigo:
Juan Perez          22333444    m   Sarmiento 123   500
Ana Acosta          24555666    f   Colon     134   650
Bartolome Barrios   27888999    m   Urquiza   479   800
```

#### Intente ingresar un registro con el valor "masculino" en el campo "sexo".
###### Un mensaje indica que el campo está definido para almacenar 1 solo caracter como máximo y está intentando ingresar 9 caracteres.

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('jorge basadre','278738999','masculino','lambayeque 479',1000);


salida del codigo:
Error en la línea de comandos : 18 Columna : 98
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "TEDDY2"."EMPLEADOS"."DOCUMENTO" (real: 9, máximo: 8)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).
```
#### Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico" Mensaje de error.
```sql

salida del codigo:

```
#### Elimine la tabla
```sql

salida del codigo:

```
