[73. Subconsultas con in ](solucionario/solucionario_73.md)
# Solucionario
# Subconsultas con in

## Ejercicios propuestos
# Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "ciudades" donde registra los nombres de las ciudades.
# Elimine las tablas "clientes" y "ciudades": 
```sql
drop table clientes;
drop table ciudades;
```
# Cree la tabla "clientes" (codigo, nombre, domicilio, ciudad, codigociudad) y "ciudades" (codigo, nombre). Agregue una restricción "primary key" para el campo "codigo" de ambas tablas y una "foreing key" para validar que el campo "codigociudad" exista en "ciudades" con eliminación en cascada:
```sql
create table ciudades(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);

create table clientes (
    codigo number(4),
    nombre varchar2(30),
    domicilio varchar2(30),
    codigociudad number(2) not null,
    primary key(codigo),
    constraint FK_clientes_ciudad
    foreign key (codigociudad)
    references ciudades(codigo)
    on delete cascade
);

```
# Ingrese algunos registros para ambas tablas: 
```sql
insert into ciudades values(1,'Cordoba');
insert into ciudades values(2,'Cruz del Eje');
insert into ciudades values(3,'Carlos Paz');
insert into ciudades values(4,'La Falda');
insert into ciudades values(5,'Villa Maria');
insert into clientes values (100,'Lopez Marcos','Colon 111',1);
insert into clientes values (101,'Lopez Hector','San Martin 222',1);
insert into clientes values (105,'Perez Ana','San Martin 333',2);
insert into clientes values (106,'Garcia Juan','Rivadavia 444',3);
insert into clientes values (107,'Perez Luis','Sarmiento 555',3);
insert into clientes values (110,'Gomez Ines','San Martin 666',4);
insert into clientes values (111,'Torres Fabiola','Alem 777',5);
insert into clientes values (112,'Garcia Luis','Sucre 888',5);

```
# Necesitamos conocer los nombres de las ciudades de aquellos clientes cuyo domicilio es en calle "San Martin", empleando subconsulta. 
```sql
SELECT nombre
FROM ciudades
WHERE codigo IN (
    SELECT codigociudad
    FROM clientes
    WHERE domicilio LIKE '%San Martin%'
);
```
# Obtenga la misma  anterior pero empleando join.
```sql
SELECT c.nombre
FROM ciudades c
JOIN clientes cl ON c.codigo = cl.codigociudad
WHERE cl.domicilio LIKE '%San Martin%';
```
# Obtenga los nombre de las ciudades de los clientes cuyo apellido no comienza con una letra específica (letra "G"), empleando subconsulta.
```sql
SELECT nombre
FROM ciudades
WHERE codigo IN (
    SELECT codigociudad
    FROM clientes
    WHERE nombre NOT LIKE 'G%'
);
```
# Pruebe la subconsulta del punto 6 separada de la consulta exterior para verificar que retorna una lista de valores de un solo campo.
```sql
SELECT codigociudad
FROM clientes
WHERE nombre NOT LIKE 'G%';
```