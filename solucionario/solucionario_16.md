[16. Ingresar algunos campos ](solucionario/solucionario_16.md)
# Solucionario  
# Ingresar algunos campos

# EJERCICIO 01 
## Primer problema: Un banco tiene registrados las cuentas corrientes de sus clientes en una tabla llamada "cuentas".

## Elimine la tabla "cuentas":

```sql
drop table cuentas;
```
## Cree la tabla :

```sql
create table cuentas(
    numero number(10) not null,
    documento char(8) not null,
    nombre varchar2(30),
    saldo number(9,2)
);
```
## Ingrese un registro con valores para todos sus campos, omitiendo la lista de campos.

```sql
insert into cuentas values ('7418926','789416','yuguiro',200);
```
## Ingrese un registro omitiendo algún campo que admita valores nulos.

```sql
insert into cuentas values (' ',' ','yuguiro',200);
```
## Verifique que en tal campo se almacenó "null"

```sql
describe cuentas;
```
## Intente ingresar un registro listando 3 campos y colocando 4 valores. Un mensaje indica que hay demasiados valores.

```sql
insert into cuentas values (' ',' ',' ','yuguiro',200);
```
## Intente ingresar un registro listando 3 campos y colocando 2 valores. Un mensaje indica que no hay suficientes valores.

```sql
insert into cuentas values (' ','yuguiro',200);
```
## Intente ingresar un registro sin valor para un campo definido "not null".

```sql
insert into cuentas values ('0','0','yuguiro',200);
```
## Vea los registros ingresados.

```sql
select * from cuentas;
```