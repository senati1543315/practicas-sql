[60. Restricciones foreign key deshabilitar y validar](solucionario/solucionario_60.md)
# Solucionario.
# Restricciones foreign key deshabilitar y validar

## Ejercicios propuestos
# Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias.
## Elimine las tablas "clientes" y "provincias":

```sql
drop table clientes;
drop table provincias;

```
## Créelas con las siguientes estructuras:

```sql
create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);

```
## Ingrese algunos registros para ambas tablas:

```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Garcia Juan','Sucre 345','Cordoba',1);
insert into clientes values(103,'Lopez Susana','Caseros 998','Posadas',3);
insert into clientes values(104,'Marcelo Moreno','Peru 876','Viedma',4);
insert into clientes values(105,'Lopez Sergio','Avellaneda 333','La Plata',5);

```
## Intente agregar una restricción "foreign key" para que los códigos de provincia de "clientes" existan en "provincias" sin especificar la opción de comprobación de datos No se puede porque al no especificar opción para la comprobación de datos, por defecto es "validate" y hay un registro que no cumple con la restricción.

```sql
ALTER TABLE clientes
ADD CONSTRAINT fk_codigoprovincia
FOREIGN KEY (codigoprovincia)
REFERENCES provincias(codigo);
```
## Agregue la restricción anterior pero deshabilitando la comprobación de datos existentes

```sql
ALTER TABLE clientes
ADD CONSTRAINT fk_codigoprovincia
FOREIGN KEY (codigoprovincia)
REFERENCES provincias(codigo)
DISABLE;
```
## Vea las restricciones de "clientes"

```sql
SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'CLIENTES';
```
## Deshabilite la restricción "foreign key" de "clientes"

```sql
ALTER TABLE clientes
DISABLE CONSTRAINT fk_codigoprovincia;
```
## Vea las restricciones de "clientes"

```sql
SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'CLIENTES';
```
## Agregue un registro que no cumpla la restricción "foreign key" Se permite porque la restricción está deshabilitada.

```sql
INSERT INTO clientes VALUES (106, 'Ana Torres', 'Sucre 999', 'Cordoba', 10);

``` 
## Modifique el código de provincia del cliente código 104 por 9 Oracle lo permite porque la restricción está deshabilitada.

```sql
UPDATE clientes SET codigoprovincia = 9 WHERE codigo = 104;

```
## Habilite la restricción "foreign key"

```sql
ALTER TABLE clientes
ENABLE CONSTRAINT fk_codigoprovincia;
```
## Intente modificar un código de provincia existente por uno inexistente

```sql
UPDATE clientes SET codigoprovincia = 10 WHERE codigo = 104;

```
## Intentealterar la restricción "foreign key" para que valide los datos existentes

```sql
ALTER TABLE clientes
MODIFY CONSTRAINT fk_codigoprovincia
ENABLE VALIDATE;
```
## Elimine los registros que no cumplen la restricción y modifique la restricción a "enable" y "validate"

```sql
DELETE FROM clientes WHERE codigoprovincia = 10;

ALTER TABLE clientes
ENABLE CONSTRAINT fk_codigoprovincia VALIDATE;
```
## Obtenga información sobre la restricción "foreign key" de "clientes"

```sql
SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'CLIENTES' AND constraint_name = 'FK_CODIGOPROVINCIA';
```