[4. Recuperar algunos campos ](solucionario/solucionario_4.md)
# Solucionario  
# Recuperar algunos campos (select)

## EJERCICIO 01
## Un videoclub que alquila películas en video almacena la información de sus películas en alquiler en una tabla llamada "peliculas".

## Elimine la tabla, si existe:

```sql
drop table peliculas;
```

```sh
Table PELICULAS dropped.
```
## Crea la tabla.

```sql
create table peliculas(
  titulo varchar(20),
  actor varchar(20),
  duracion integer,
  cantidad integer
);
```

```sh
Table PELICULAS created.
```
## Vea la estructura de la tabla:

```sql
describe peliculas;
```

```sh
Name     Null? Type         
-------- ----- ------------ 
TITULO         VARCHAR2(20) 
ACTOR          VARCHAR2(20) 
DURACION       NUMBER(38)   
CANTIDAD       NUMBER(38) 
```
## Ingrese los siguientes registros:

```sql
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,2);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',90,2);
```

```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## Realice un "select" mostrando solamente el título y actor de todas las películas:

```sql
select titulo,actor from peliculas;
```

```sh
TITULO               ACTOR               
-------------------- --------------------
Mision imposible     Tom Cruise          
Mision imposible 2   Tom Cruise          
Mujer bonita         Julia R.            
Elsa y Fred          China Zorrilla 
```
## Muestre el título y duración de todas las peliculas.

```sql
select titulo,duracion from peliculas;
```

```sh
TITULO                 DURACION
-------------------- ----------
Mision imposible            120
Mision imposible 2          180
Mujer bonita                 90
Elsa y Fred                  90
```
## Muestre el título y la cantidad de copias.

```sql
select titulo, cantidad from peliculas;
```

```sh
TITULO                 CANTIDAD
-------------------- ----------
Mision imposible              3
Mision imposible 2            2
Mujer bonita                  3
Elsa y Fred                   2

``` 

# EJERCICIO 02.

## Una empresa almacena los datos de sus empleados en una tabla llamada "empleados".

## Elimine la tabla, si existe:

```sql
drop table empleados;
```

```sh
Error starting at line : 1 in command -
drop table empleados
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
## Cree la tabla:

```sql
 create table empleados(
  nombre varchar(20),
  documento varchar(8), 
  sexo varchar(1),
  domicilio varchar(30),
  sueldobasico float
 );
```

```sh
Table EMPLEADOS created.
```
## Vea la estructura de la tabla:

```sql
Vea la estructura de la tabla:
```

```sh
Name         Null? Type         
------------ ----- ------------ 
NOMBRE             VARCHAR2(20) 
DOCUMENTO          VARCHAR2(8)  
SEXO               VARCHAR2(1)  
DOMICILIO          VARCHAR2(30) 
SUELDOBASICO       FLOAT(126) 
```
## Ingrese algunos registros:

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22345678','m','Sarmiento 123',300);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24345678','f','Colon 134',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Marcos Torres','27345678','m','Urquiza 479',800);

```

```sh
1 row inserted.


1 row inserted.


1 row inserted.
```

## Muestre todos los datos de los empleados.

```sql
select * from empleados;
```

```sh
NOMBRE               DOCUMENT S DOMICILIO                      SUELDOBASICO
-------------------- -------- - ------------------------------ ------------
Juan Perez           22345678 m Sarmiento 123                           300
Ana Acosta           24345678 f Colon 134                               500
Marcos Torres        27345678 m Urquiza 479                             800
```
## Muestre el nombre y documento de los empleados.

```sql
select nombre,documento from empleados;
```

```sh
NOMBRE               DOCUMENT
-------------------- --------
Juan Perez           22345678
Ana Acosta           24345678
Marcos Torres        27345678
```
## Realice un "select" mostrando el nombre, documento y sueldo básico de todos los empleados.

```sql
select nombre,documento, sueldobasico from empleados;
```

```sh
NOMBRE               DOCUMENT SUELDOBASICO
-------------------- -------- ------------
Juan Perez           22345678          300
Ana Acosta           24345678          500
Marcos Torres        27345678          800
```

# ejercicio 03 
#  Un comercio que vende artículos de computación registra la información de sus productos en la tabla llamada "articulos".
## Elimine la tabla si existe:

```sql
drop table if exists articulos;

```

```sh
Error starting at line : 1 in command -
drop table if exists articulos
Error report -
ORA-00933: SQL command not properly ended
00933. 00000 -  "SQL command not properly ended"
*Cause:    
*Action:
## 02 Cree la tabla "articulos" con los campos necesarios para almacenar los siguientes datos:
```
```sql
create table articulos (
     codigo int, 
     nombre varchar2(20),  
     descripcion varchar2(30),
     precio float
);
```

```sh
Table ARTICULOS created.
```
## Vea la estructura de la tabla (describe).

```sql
describe articulos;
```

```sh
Name        Null? Type         
----------- ----- ------------ 
CODIGO            NUMBER(38)   
NOMBRE            VARCHAR2(20) 
DESCRIPCION       VARCHAR2(30) 
PRECIO            FLOAT(126)  
``` 
## Ingrese algunos registros:

```sql
 insert into articulos (codigo, nombre, descripcion, precio)
  values (1,'impresora','Epson Stylus C45',400.80);
 insert into articulos (codigo, nombre, descripcion, precio)
  values (2,'impresora','Epson Stylus C85',500);
 insert into articulos (codigo, nombre, descripcion, precio)
  values (3,'monitor','Samsung 14',800);
```

```sh
1 row inserted.


1 row inserted.


1 row inserted.

``` 
## Muestre todos los campos de todos los registros.

```sql
select * from articulos;
```

```sh
    CODIGO NOMBRE               DESCRIPCION                        PRECIO
---------- -------------------- ------------------------------ ----------
         1 impresora            Epson Stylus C45                    400.8
         2 impresora            Epson Stylus C85                      500
         3 monitor              Samsung 14                            800
``` 
## Muestre sólo el nombre, descripción y precio.

```sql
select nombre,descripcion,precio from articulos;
```

```sh
NOMBRE               DESCRIPCION                        PRECIO
-------------------- ------------------------------ ----------
impresora            Epson Stylus C45                    400.8
impresora            Epson Stylus C85                      500
monitor              Samsung 14                            800
```