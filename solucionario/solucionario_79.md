# [1.](solucionario/solucionario_79.md).
## Practicas de laboratorio

### Eliminamos las tablas:
```sql
drop table alumnos;
drop table aprobados;

```
## Creamos las tablas:
```sql
create table alumnos(
    documento char(8) not null,
    nombre varchar2(30),
    nota number(4,2)
    constraint CK_alumnos_nota_valores check (nota>=0 and nota <=10),
    primary key(documento)
);

create table aprobados(
    documento char(8) not null,
    nota number(4,2)
    constraint CK_aprobados_nota_valores check (nota>=0 and nota <=10),
    primary key(documento)
);

```
## Ingresamos registros en "alumnos":
```sql

insert into alumnos values('30000000','Ana Acosta',8);
insert into alumnos values('30111111','Betina Bustos',9);
insert into alumnos values('30222222','Carlos Caseros',2.5); 
insert into alumnos values('30333333','Daniel Duarte',7.7);
insert into alumnos values('30444444','Estela Esper',3.4);
```


## Ingresamos registros en la tabla "aprobados" seleccionando registros de la tabla "alumnos":
```sql
insert into aprobados 
select documento,nota
from alumnos
where nota>=4;
```


## Note que no se listan los campos en los cuales se cargan los datos porque tienen el mismo nombre que los de la tabla de la cual extraemos la información.
## Veamos si los registros se han cargado:
```sql
select *from aprobados;
```

## Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:
```sql

drop table alumnos;
drop table aprobados;

create table alumnos(
    documento char(8) not null,
    nombre varchar2(30),
    nota number(4,2)
    constraint CK_alumnos_nota_valores check (nota>=0 and nota <=10),
    primary key(documento)
);

create table aprobados(
    documento char(8) not null,
    nota number(4,2)
    constraint CK_aprobados_nota_valores check (nota>=0 and nota <=10),
    primary key(documento)
);

insert into alumnos values('30000000','Ana Acosta',8);
insert into alumnos values('30111111','Betina Bustos',9);
insert into alumnos values('30222222','Carlos Caseros',2.5); 
insert into alumnos values('30333333','Daniel Duarte',7.7);
insert into alumnos values('30444444','Estela Esper',3.4);

 -- Ingresamos registros en la tabla "aprobados" 
 -- seleccionando registros de la tabla "alumnos":
insert into aprobados 
select documento,nota
from alumnos
where nota>=4;

 

select *from aprobados;

```

## Ejercicios propuestos
## Un comercio que vende artículos de librería y papelería almacena la información de sus ventas en una tabla llamada "facturas" y otra "clientes".

## Elimine las tablas:

```sql
 
drop table facturas cascade constraints;
drop table clientes;
```


## Créelas:
```sql


create table clientes(
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo)
);

create table facturas(
    numero number(6) not null,
    fecha date,
    codigocliente number(5) not null,
    total number(6,2),
    primary key(numero),
    constraint FK_facturas_cliente
    foreign key (codigocliente)
    references clientes(codigo)
);

```

## Ingrese algunos registros:
```sql

insert into clientes values(1,'Juan Lopez','Colon 123');
insert into clientes values(2,'Luis Torres','Sucre 987');
insert into clientes values(3,'Ana Garcia','Sarmiento 576');
insert into clientes values(4,'Susana Molina','San Martin 555');
insert into facturas values(1200,'15/04/2017',1,300);
insert into facturas values(1201,'15/04/2017',2,550);
insert into facturas values(1202,'15/04/2017',3,150);
insert into facturas values(1300,'20/04/2017',1,350);
insert into facturas values(1310,'22/04/2017',3,100);

```

## El comercio necesita una tabla llamada "clientespref" en la cual quiere almacenar el nombre y domicilio de aquellos clientes que han comprado hasta el ## ## ## momento más de 500 pesos en mercaderías. Elimine la tabla y créela con esos 2 campos:


## drop table clientespref;
 ```sql
create table clientespref(
    nombre varchar2(30),
    domicilio varchar2(30)
);

```