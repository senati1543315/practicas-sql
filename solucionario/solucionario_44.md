[44. Indices (Crear . Información) ](solucionario/solucionario_44.md)
## ejercicios de laboratorio
## ejercicios 1
## 1
```sql
 drop table alumnos;
 create table alumnos(
  legajo char(5) not null,
  documento char(8) not null,
  nombre varchar(30),
  curso char(1),
  materia varchar2(30),
  notafinal number(4,2)
 );
```
## 2
```sql
 insert into alumnos values ('A123','22222222','Perez Patricia','5','Matematica',9);
 insert into alumnos values ('A234','23333333','Lopez Ana','5','Matematica',9);
 insert into alumnos values ('A345','24444444','Garcia Carlos','6','Matematica',8.5);
 insert into alumnos values ('A348','25555555','Perez Patricia','6','Lengua',7.85);
 insert into alumnos values ('A457','26666666','Perez Fabian','6','Lengua',3.2);

```
## 3
```sql

 create unique index I_alumnos_nombre
 on alumnos(nombre);

```
## 4
```sql
 create index I_alumnos_nombre
 on alumnos(nombre); 
```
## 5
```sql
create index I_alumnos_legajo
 on alumnos(legajo); 

```
## 6
```sql
alter table alumnos
  add constraint PK_alumnos_legajo
  primary key (legajo);

```
## 7
```sql
 select constraint_name, constraint_type, index_name
  from user_constraints
  where table_name='ALUMNOS';

```
## 8
```sql
 alter table alumnos
  add constraint UQ_alumnos_documento
  unique (documento);
```

## 9
```sql
 select constraint_name, constraint_type, index_name
  from user_constraints
  where table_name='ALUMNOS';
```
## 10
```sql
 create unique index I_alumnos_notafinal
 on alumnos(notafinal); 

```
## 11
```sql
 create index I_alumnos_notafinal
 on alumnos(notafinal); 

```
## 12
```sql
 create index I_alumnos_cursomateria
 on alumnos(curso,materia);
```
## 13
```sql
 create unique index I_alumnos_materia
  on alumnos(materia); 

```
## 14
```sql

 select index_name, index_type, uniqueness
  from user_indexes
  where table_name='ALUMNOS';
```
## 15
```sql
select index_name,column_name,column_position
  from user_ind_columns
  where table_name='ALUMNOS';
```
## 16
```sql

 select *from user_objects
  where object_type='INDEX' and
  object_name like '%ALUMNOS%';
```