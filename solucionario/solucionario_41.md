[41. Restricciones: información ](solucionario/solucionario_41.md)


## Ejercicios propuestos
## Primer problema:
## Una empresa tiene registrados datos de sus empleados en ## ## una tabla llamada "empleados".
## 1- Elimine la tabla:
```sql

  drop table empleados;
  ```
## 2- Créela con la siguiente estructura e ingrese los registros siguientes:
```sql
 create table empleados (
  codigo number(6),
  documento char(8),
  nombre varchar2(30),
  seccion varchar2(20),
  sueldo number(6,2)
 );

 insert into empleados
  values (1,'22222222','Alberto Acosta','Sistemas',-10);
 insert into empleados
  values (2,'33333333','Beatriz Benitez','Recursos',3000);
 insert into empleados
  values (3,'34444444','Carlos Caseres','Contaduria',4000);
  ```
## 3-  Intente agregar una restricción "check" para 
## asegurarse que no se ingresen valores negativos para el ## ## sueldo sin especificar validación ni estado:
```sql
 alter table empleados
 add constraint CK_empleados_sueldo_positivo
 check (sueldo>=0);
 ```
## 4
```sql
 insert into empleados
  values (4,'35555555','Daniel Duarte','Administracion',-2000);
```
## 5
```sql
 alter table empleados
  disable novalidate
  constraint CK_empleados_sueldo_positivo;
```
## 6
```sql
 insert into empleados
  values (4,'35555555','Daniel Duarte','Administracion',-2000);
```
## 7
```sql
alter table empleados
 add constraint CK_empleados_seccion_lista
 check (seccion in ('Sistemas','Administracion','Contaduria'));
```
## 8
```sql
 alter table empleados
 add constraint CK_empleados_seccion_lista
 check (seccion in ('Sistemas','Administracion','Contaduria')) novalidate;
```
## 9
```sql
 select constraint_name, constraint_type, status, validated
  from user_constraints
  where table_name='EMPLEADOS';
```
## 10
```sql
alter table empleados
  enable novalidate constraint CK_empleados_sueldo_positivo;
```

## 11
```sql
 update empleados set seccion='Recursos' where nombre='Carlos Caseres';
```
## 12
```sql
alter table empleados
  disable novalidate constraint CK_empleados_seccion_lista;
```
## 13
```sql

```
## 14
```sql
alter table empleados
 add constraint PK_empleados_codigo
 primary key (codigo) disable;
```
## 15 
```sql
 insert into empleados
  values (4,'22333444','Federico Fuentes',null,null);
```
## 16 
```sql
alter table empleados
  enable novalidate constraint PK_empleados_codigo
```
## 17 
```sql
update empleados set codigo=5 where nombre='Federico Fuentes';
```
## 18 
```sql
 alter table empleados
  enable novalidate constraint PK_empleados_codigo;
```
## 19
```sql
alter table empleados
 add constraint UQ_empleados_documento
 unique(documento);
```
## 20
```sql
select constraint_name, constraint_type, status, validated
  from user_constraints
  where table_name='EMPLEADOS';

```
## 21
```sql
 alter table empleados
  disable constraint PK_empleados_codigo;
 alter table empleados
  disable constraint UQ_empleados_documento;
 alter table empleados
  disable constraint CK_empleados_sueldo_positivo;
 alter table empleados
  disable constraint CK_empleados_seccion_lista;
```
## 22 
```sql
 insert into empleados
  values (1,'33333333','Federico Fuentes','Gerencia',-1500);
```
## 23
```sql
alter table empleados
  enable novalidate constraint CK_empleados_sueldo_positivo;
```
## 24
```sql
 alter table empleados
  enable novalidate constraint CK_empleados_seccion_lista;
```
## 25
```sql
 alter table empleados
  enable novalidate constraint PK_empleados_codigo;
```
## 26
```sql
 alter table empleados
  enable novalidate constraint UQ_empleados_documento;
```
## 27
```sql
delete empleados where seccion='Gerencia';
```
## 28
```sql
 alter table empleados
  enable novalidate constraint PK_empleados_codigo;
 alter table empleados
  enable novalidate constraint UQ_empleados_documento;
```


