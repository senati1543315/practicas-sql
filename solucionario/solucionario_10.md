[10. Valores nulos](solucionario/solucionario_10.md)
# Solucionario  
# Valores nulos (null).

# EJERCICIO 01.
## Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".
## Elimine la tabla y créela con la siguiente estructura:

```sql
 drop table medicamentos;
 create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);
```

```sh
Table MEDICAMENTOS dropped.
```
## Visualice la estructura de la tabla "medicamentos" note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".

```sql
describe medicamentos;
```

```sh
Name        Null?    Type         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3)   
```
## Ingrese algunos registros con valores "null" para los campos que lo admitan:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);

```

```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## Vea todos los registros.

```sql
select * from medicamentos;
```

```sh
    CODIGO NOMBRE               LABORATORIO              PRECIO   CANTIDAD
---------- -------------------- -------------------- ---------- ----------
         1 Sertal gotas                                                100
         2 Sertal compuesto                                 8.9        150
         3 Buscapina            Roche                                  200  
```
## Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(9,'paracetamol','',0,250);

```

```sh
1 row inserted.
```
## Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(9,'','inkafarma',0,250);
```