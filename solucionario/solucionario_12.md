[12. Clave primaria ](solucionario/solucionario_12.md)
# Solucionario  
# Clave primaria (primary key)

# Ejeercicio 01 
## Trabaje con la tabla "libros" de una librería.
 
## Elimine la tabla:

```sql
drop table libros;
```

```sh
Table LIBROS dropped.
```
## Créela con los siguientes campos, estableciendo como clave primaria el campo "codigo":

```sql
create table libros(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    autor varchar2(20),
    editorial varchar2(15),
    primary key (codigo)
);
```

```sh
Table LIBROS created.
```
## Ingrese los siguientes registros:

```sql
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece');
insert into libros (codigo,titulo,autor,editorial) values (2,'Martin Fierro','Jose Hernandez','Planeta');
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo');
```

```sh
1 row inserted.


1 row inserted.


1 row inserted.
```
## Ingrese un registro con código repetido (aparece un mensaje de error)

```sql
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece');
```

```sh
Error starting at line : 1 in command -
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece')
Error report -
ORA-00001: unique constraint (YUGUIRO.SYS_C008397) violated
```
## Intente ingresar el valor "null" en el campo "codigo"

```sql
insert into libros (codigo,titulo,autor,editorial) values (null,'El aleph','Borges','Emece');
```

```sh
Error starting at line : 1 in command -
insert into libros (codigo,titulo,autor,editorial) values (null,'El ph','Bores','Emece')
Error at Command Line : 1 Column : 60
Error report -
SQL Error: ORA-01400: cannot insert NULL into ("YUGUIRO"."LIBROS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
## Intente actualizar el código del libro "Martin Fierro" a "1" (mensaje de error)

```sql
UPDATE libros SET codigo = '1' WHERE titulo = 'Martin Fierro';
```

```sh
Error starting at line : 1 in command -
UPDATE libros SET codigo = '1' WHERE titulo = 'Martin Fierro'
Error report -
ORA-00001: unique constraint (YUGUIRO.SYS_C008397) violated
```
## Actualice el código del libro "Martin Fierro" a "10"

```sql
UPDATE libros SET codigo = '10' WHERE titulo = 'Martin Fierro';
```

```sh
1 row updated.
```
## Vea qué campo de la tabla "LIBROS" fue establecido como clave primaria

```sql
SHOW KEYS FROM LIBROS WHERE Key_name = 'PRIMARY';
```

```sh
SP2-0158: unknown SHOW option "KEYS"
SP2-0158: unknown SHOW option "FROM"
SP2-0158: unknown SHOW option "LIBROS"
SP2-0158: unknown SHOW option "WHERE"
SP2-0158: unknown SHOW option "Key_name"
SP2-0158: unknown SHOW option "="
SP2-0158: unknown SHOW option "'PRIMARY'"
```
## Vea qué campo de la tabla "libros" (en minúsculas) fue establecido como clave primaria

```sql
SELECT cols.column_name
FROM all_constraints cons
JOIN all_cons_columns cols ON cons.constraint_name = cols.constraint_name
WHERE cons.table_name = 'libros' AND cons.constraint_type = 'P';

```

```sh
no rows selected
```
# EJERCICIO 02 
## Un instituto de enseñanza almacena los datos de sus estudiantes en una tabla llamada "alumnos".

## Elimine la tabla "alumnos":

```sql
drop table alumnos;
```

```sh
Error starting at line : 1 in command -
drop table alumnos
Error report -
ORA-00942: table or view does not exist
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
## Cree la tabla con la siguiente estructura intentando establecer 2 campos como clave primaria, el campo "documento" y "legajo":

```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo),
    primary key(documento)
);

```

```sh
Error starting at line : 1 in command -
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo),
    primary key(documento)
)
Error report -
ORA-02260: table can have only one primary key
02260. 00000 -  "table can have only one primary key"
*Cause:    Self-evident.
*Action:   Remove the extra primary key.
```
## Cree la tabla estableciendo como clave primaria el campo "documento":

```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

```

```sh
Error starting at line : 1 in command -
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
)
Error report -
ORA-00955: name is already used by an existing object
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:
```
## Verifique que el campo "documento" no admite valores nulos

```sql
SELECT nullable
FROM user_tab_columns
WHERE table_name = 'Alumnos' AND column_name = 'documento';
```

```sh
no rows selected
``` 
## Ingrese los siguientes registros:

```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A233','22345345','Perez Mariana','Colon 234');
insert into alumnos (legajo,documento,nombre,domicilio) values('A567','23545345','Morales Marcos','Avellaneda 348');

```

```sh
1 row inserted.
```
## Intente ingresar un alumno con número de documento existente (no lo permite)

```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A133','22345345','Romel','Colon 234');

```

```sh
1 row inserted.
```
## Intente ingresar un alumno con documento nulo (no lo permite)

```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A133',null,'Romel','Colon 234');

```

```sh
Error starting at line : 1 in command -
insert into alumnos (legajo,documento,nombre,domicilio) values('A133',null,'Romel','Colon 234')
Error at Command Line : 1 Column : 71
Error report -
SQL Error: ORA-01400: cannot insert NULL into ("YUGUIRO"."ALUMNOS"."DOCUMENTO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.

```
## Vea el campo clave primaria de "ALUMNOS".

```sql
select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='ALUMNOS';

```