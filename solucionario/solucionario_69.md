[69. Eliminar campos (alter table - drop) ](solucionario/solucionario_69.md)
# Solucionario
# Eliminar campos (alter table - drop)

## Ejercicios propuestos

Trabaje con una tabla llamada "empleados" y "secciones".
## Elimine las tablas y créelas:

```sql
drop table empleados;
drop table secciones;

create table secciones(
    codigo number(2),
    nombre varchar(20),
    primary key (codigo)
);

create table empleados(
    apellido varchar2(20),
    nombre varchar2(20) not null,
    domicilio varchar2(30),
    seccion number(2),
    sueldo number(8,2),
    constraint CK_empleados_sueldo
    check (sueldo>=0) disable,
    fechaingreso date,
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo)
    on delete set null
 );
```

## Ingrese algunos registros en ambas tablas:

```sql
insert into secciones values(8,'Secretaria');
insert into secciones values(9,'Contaduria');
insert into secciones values(10,'Sistemas');
insert into empleados values('Lopez','Juan','Colon 123',8,505.50,'10/10/1980');
insert into empleados values('Gonzalez','Juana','Avellaneda 222',9,600,'01/05/1990');
insert into empleados values('Perez','Luis','Caseros 987',10,800,'12/09/2000');
```

## Elimine el campo "domicilio" y luego verifique la eliminación
```sql
alter table empleados
drop column domicilio;

```
## Vea las restricciones de "empleados" (1 restricción "foreign key" y 2 "check")
```sql
describe empleados;
select *from user_constraints
where table_name='EMPLEADOS';
```
## Intente eliminar el campo "codigo" de "secciones"
```sql
alter table secciones
drop column codigo;
```
## Elimine la restricción "foreign key" de "empleados", luego elimine el campo "codigo" de "secciones" y verifique la eliminación
```sql
alter table empleados
drop constraint FK_empleados_seccion;
```
## Verifique que al eliminar el campo "codigo" de "secciones" se ha eliminado la "primary key" de "secciones"
```sql
alter table secciones
drop column codigo; 
```
## Elimine el campo "sueldo" y verifique que la restricción sobre tal campo se ha eliminado
```sql

alter table empleados
drop column sueldo;
```
## Cree un índice no único por el campo "apellido" y verifique su existencia consultando "user_indexes"
```sql
create index I_empleados_apellido
on empleados(apellido);
```
## Elimine el campo "apellido" y verifique que el índice se ha eliminado
```sql
drop index idx_apellido;

alter table empleados
drop COLUMN apellido;
```
## Elimine 2 campos de "empleados" y vea la estructura de la tabla
```sql
alter table empleados
drop COLUMN domicilio,
drop COLUMN telefono;
```
## Intente eliminar el único campo de "empleados"
```sql
alter table empleados
drop column nombre;
```