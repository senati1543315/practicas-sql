[8. Actualizar registros ](solucionario/solucionario_8.md)
# Solucionario  
# Actualizar registros (update)

# EJERCICIO 01.

## Trabaje con la tabla "agenda" que almacena los datos de sus amigos.

## Elimine la tabla y créela con la siguiente estructura:

```sql
drop table agenda;

create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```

```sh
Table AGENDA dropped.


Table AGENDA created.
```
## Ingrese los siguientes registros:

```sql
insert into agenda (apellido,nombre,domicilio,telefono) values ('Acosta','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Suarez','Susana','Gral. Paz 1234','4123456');

```

```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## Modifique el registro cuyo nombre sea "Juan" por "Juan Jose" (1 registro actualizado)

```sql
UPDATE agenda SET nombre = 'Juan Jose' WHERE nombre = 'Juan';
```

```sh
1 row updated.
```
## Actualice los registros cuyo número telefónico sea igual a "4545454" por "4445566" (2 registros)

```sql
UPDATE agenda SET telefono = '4445566' WHERE telefono = '4545454';
```

```sh
2 rows updated.
```
## Actualice los registros que tengan en el campo "nombre" el valor "Juan" por "Juan Jose" (ningún registro afectado porque ninguno cumple con la condición del "where")

```sql
UPDATE agenda SET nombre = 'Juan Jose' WHERE nombre = 'Juan';

```

```sh
0 rows updated.
```
# EJERCICIO 02.
## Trabaje con la tabla "libros" de una librería.
## Elimine la tabla y créela con los siguientes campos: titulo (cadena de 30 caracteres de longitud), autor (cadena de 20), editorial (cadena de 15) y precio (entero no mayor a 999.99):

```sql
drop table libros; 
create table libros (
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);
```

```sh
Table LIBROS dropped.

Table LIBROS created.
```
## Ingrese los siguientes registros:

```sql
insert into libros (titulo, autor, editorial, precio) values ('El aleph','Borges','Emece',25.00);
insert into libros (titulo, autor, editorial, precio) values ('Martin Fierro','Jose Hernandez','Planeta',35 50);
insert into libros (titulo, autor, editorial, precio) values ('Aprenda PHP','Mario Molina','Emece',45.50);
insert into libros (titulo, autor, editorial, precio) values ('Cervantes y el quijote','Borges','Emece',25);
insert into libros (titulo, autor, editorial, precio) values ('Matematica estas ahi','Paenza','Siglo XXI',15);
```

```sh
1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.


1 row inserted.
```
## Muestre todos los registros (5 registros)

```sql
select * from libros;
```

```sh
TITULO AUTOR EDITORIAL PRECIO
```
## Modifique los registros cuyo autor sea igual a "Paenza", por "Adrian Paenza" (1 registro)

```sql
update libros set autor='Adrian Paenza' where autor='Paenza';
```

```sh
1 rows updated.
```
## Nuevamente, modifique los registros cuyo autor sea igual a "Paenza", por "Adrian Paenza" (ningún registro afectado porque ninguno cumple la condición)

```sql
update libros set autor='Adrian Paenza' where autor='Paenza';
```

```sh
0 rows update.
```
## Actualice el precio del libro de "Mario Molina" a 27 pesos (1 registro)

```sql
update libros set precio = '27' where autor= 'Mario Molina';
```

```sh
1 rows update
```
## Actualice el valor del campo "editorial" por "Emece S.A.", para todos los registros cuya editorial sea igual a "Emece" (3 registros)

```sql
update editorial set Emece_S.A.;
```